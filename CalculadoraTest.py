__author__ = 'gloria'

import unittest
from Calculadora import Calculadora


class CalculadoraTestCase(unittest.TestCase):

    def test_sumar_vacia(self):
        self.assertEqual(Calculadora().sumar(""),0,"Cadena vacia")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un numero")

    def test_sumar_cadenaConDosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2"),3,"Dos Numeros")

    def test_sumar_cadenaMasdeDos(self):
        self.assertEqual(Calculadora().sumar("1,2,3"),6,"Dos Numeros")

    def test_sumar_cadenaMasDeDos2(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4"),10,"Dos Numeros")

    def test_sumar_cadenaSeparador1(self):
        self.assertEqual(Calculadora().sumar("1:2"),3,"Dos Numeros")

    def test_sumar_cadenaSeparador2(self):
        self.assertEqual(Calculadora().sumar("1&2"),3,"Dos Numeros")

if __name__ == '__main__':
    unittest.main()
