#Kata TDD 001- Python
##Objetivo
Implementar una calculadora de Strings
[Kata propuesta por Roy Osherove] (http://osherove.com/tdd-kata-1/)

##Primera versión:
La calculadora tiene un metodo **sumar** que recibe un string vacio, un string con un número o un string con dos numeros separados por coma.

Condiciones de la Kata
- Solo se prueba para datos de entrada válidos

###Paso 1 - Que funcione para cadena vacia
1. (Rojo) Crear una prueba para cuando la cadena es vacia
2. (Verde) Se escribe el método para que la prueba corra

###Paso 2 - Que funcione para cadena con un numero (pruebas (1) un numero (2) que funcione con dos distintos)
1. (Rojo) Se agrega una prueba para cuando la cadena trae un numero
2. (Verde) Se corrige el código para que corra la prueba
3. (Rojo) Se agrega prueba para probar con otro numero diferente
4. (Verde) Se corrige el código para que la prueba corra

